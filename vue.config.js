const path = require("path");
const PrerenderSpaPlugin = require("prerender-spa-plugin");
const NAME = require("./package.json").name;

const Renderer = PrerenderSpaPlugin.PuppeteerRenderer;

const isProduction = process.env.NODE_ENV === "production";
const isDefault = process.env.NODE_ENV === "default";
const isRelease = process.env.NODE_ENV === "release";

module.exports = {
  publicPath: isDefault
    ? "/" + NAME + "/"
    : isRelease
    ? "https://bizdamienlu.gitlab.io/" + NAME + "/"
    : "/",
  outputDir: "dist",
  assetsDir: "static",
  productionSourceMap: false,
  configureWebpack: (config) => {
    if (isProduction || isRelease) {
      config.plugins.push(
        // 預渲染外掛配置
        new PrerenderSpaPlugin({
          // 靜態資源路徑
          staticDir: path.join(__dirname, "/dist"),
          // 預渲染路由
          routes: ["/"],
          renderer: new Renderer({
            renderAfterDocumentEvent: "render-event",
            // headless: false,
          }),
          args: ["--disable-setuid-sandbox", "--no-sandbox"],
        })
      );
    }
  },
};
